	AREA	GPIO, CODE, READWRITE	
	EXPORT lab4
	IMPORT read_string
	IMPORT output_string
	IMPORT uart_init
	IMPORT pin_connect_block_setup_for_uart0
	IMPORT hex
		
PIODATA EQU 0x8 ; Offset to parallel I/O data register

;---------------------------------------------------------------
;display prompts
;---------------------------------------------------------------
string = " ",0
buffer = 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 
instruction	= "type the string.  if there is an h, an h will appear on the 7-seg.  otherwise '-'",0xA, 0xD, 0
answer1 = " ", 0
answer2 = " ", 0
	ALIGN

;****************************************************************
;TRANSITIONS FROM LAB4WRAPPER TO HERE!!!
;****************************************************************

lab4
	STMFD SP!,{lr}	; Store register lr on stack
	BL pin_connect_block_setup_for_uart0
	BL uart_init
	LDR r10, =instruction
	BL output_string

	LDR r1, =0xE0028008	; direction address
	LDR r2, =0x00003F80	;this number is not wrong anymore
	STR r2, [r1] ;
	LDR r3, =0xE0028000 ; Base address
	MOV r1, #0

	LDR r10, =string	;gets the address for the input string
	BL read_string
mainloop
	LDRB r0, [r10, r1]
	CMP r0, #0x00
	BEQ programDone
	CMP r0, #'h'
	BEQ outputH
	add r1, r1, #1
	B mainloop
outputH
	LDR r2, =0x00003A00 ; h
	STR r2, [r3, #4] ; Display (0x4 = offset to IOSET)
	B Done      

programDone
	LDR r2, =0x00002000 ; -
	STR r2, [r3, #4] ; Display (0x4 = offset to IOSET)
Done
	LDMFD SP!, {lr}
	END
	