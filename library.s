	AREA	lib, CODE, READWRITE
	EXPORT pin_connect_block_setup_for_uart0
	EXPORT	uart_init
	EXPORT	hex
	EXPORT read_string
	EXPORT output_string
	EXPORT parse_int
	EXPORT clearDisp
	EXPORT turnOFF
	EXPORT turnON
	EXPORT randomNum
	EXPORT interrupt_init
	EXPORT create_board
	EXPORT render_and_draw
	EXPORT move_bomberman
	EXPORT lose_life
	EXPORT move_enemy
	EXPORT move_enemy2
	EXPORT move_boss
	EXPORT random_init
	EXPORT random
	EXPORT random2
	EXPORT bombCheck
	EXPORT timerCheck
	EXPORT led
	EXPORT game_setup
	EXPORT generate_brick
	EXPORT pausedScreen
	EXPORT level_up
	EXPORT score_up
	IMPORT endGame


StatusUP = 0xA, 0xD, "Time: 120                          Score: 0000", 0
board = 0xA, 0xD,  "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ" 
	  = 0xA, 0xD,  "Z                                             Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
      = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z                                             Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z"  
	  = 0xA, 0xD,  "Z                                             Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z                                             Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z                                             Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
	  = 0xA, 0xD,  "Z   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   ZZZ   Z" 
      = 0xA, 0xD,  "Z                                             Z" 
      = 0xA, 0xD,  "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ", 0	

;///////////////////////////////////////////////////////
; HEX MAP
;///////////////////////////////////////////////////////
digits_SET
		DCD 0x00001F80  ; 0
 		DCD 0x00000300  ; 1
		DCD 0x00002D80  ; 2
		DCD 0x00002780  ; 3
		DCD 0x00003300  ; 4
		DCD 0x00003680  ; 5
		DCD 0x00003E80  ; 6
		DCD 0x00000380  ; 7
		DCD 0x00003F80  ; 8
		DCD 0x00003380  ; 9
		DCD 0x00003B80  ; A
		DCD 0x00003E00  ; B
		DCD 0x00001C80  ; C
		DCD 0x00002F00  ; D
		DCD 0x00003C80  ; E
		DCD 0x00003880  ; F
	ALIGN
;///////////////////////////////////////////////////////

;-------------------------------------------------------------------------------
;overlapping prompts
;-------------------------------------------------------------------------------
string	= " ", 0
wall = "###"
	ALIGN
;-------------------------------------------------------------------------------
;clearDisp
;-------------------------------------------------------------------------------
clear1 = 0xC, "Clearing the 7-segment to zero...", 0xA, 0
	ALIGN
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;turn ON / OFF
;-------------------------------------------------------------------------------
turnONprompt = 0xC, "Turning on Display...", 0xA, 0
turnOFFprompt = 0xC, "Turning OFF Display...", 0xA, 0
save = 0x1B, "[7", 0
reset = 0x1B, "[8", 0
turnOFFprompt2 = 0xC, "Keep in mind that any changes will still occur while the 7-seg is turned off", 0xA, 0
lolife = 0xA, 0xD, "You lost a life!", 0xA, 0xD, 0
intro1 = 0xA, 0xC,"Welcome to CSE-380 rendition of 'Bomberman!!",0xA,0xD, "Use w, a, s ,d to move bomberman.", 0
introP = 0xA, 0xD,"Welcome to CSE-380 rendition of 'Bomberman!!",0xA,0xD, "Use w, a, s ,d to move bomberman.", 0
intro2 = 0xA,0xD,"Press the spacebar to place the bomb on bomberman's current position" 
timeCount = " ", 0
scoreCount = " ",0
	ALIGN
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
;Enemy Positions
;-------------------------------------------------------------------------------
position = "         ", 0
positionEnemy1 = "         ", 0
positionEnemy2 = "         ", 0
positionBoss = "         ", 0
enemy1Dir = "         ", 0
enemy2Dir = "         ", 0
bossDir = "         ", 0

;-------------------------------------------------------------------------------
;bomb position and explosion
;-------------------------------------------------------------------------------
bombposition = " ", 0
bombflag = " ", 0

;-------------------------------------------------------------------------------
;game info
;-------------------------------------------------------------------------------
lives = " ", 0
enemycount = " ", 0
gameover1 = 0xA, 0xD, "Gameover", 0
pausedNOTE = 0xA, 0xD, "*-PAUSED-*", 0
level = " ", 0
bombTimer = " ", 0
brickcount = "        ",0

;-------------------------------------------------------------------------------
;randomNum
;-------------------------------------------------------------------------------
random1 = 0xC, "Displaying random number on the 7-segment display...", 0xA, 0
rand = "         ", 0
rand2 = "         ", 0
	ALIGN
;-------------------------------------------------------------------------------

pin_connect_block_setup_for_uart0
	STMFD sp!, {r0, r1, lr}
	LDR r0, =0xE002C000  ; PINSEL0
	LDR r1, [r0]
	ORR r1, r1, #5
	BIC r1, r1, #0xA
	STR r1, [r0]
	LDMFD sp!, {r0, r1, lr}
	BX lr

;===============================================================
;	UART INIT
;===============================================================
uart_init
	STMFD sp!, {r0, r1,  lr}
;8-bit word length, 1 stop bit, no parity,
;Disable break control
;Enable divisor latch access
	LDR r0, =0xE000C00C			;
	MOV r1, #131				;
	STRB r1, [r0]				;
;Set lower divisor latch for 1152000 baud
   	LDR r0, =0xE000C000			;
	MOV r1, #1				;
	STRB r1, [r0]				;
;Set upper divisor latch for 1152000 baud
	LDR r0, =0xE000C004			;
	MOV r1, #0					;
	STRB r1, [r0]				;

 	LDR r0, =0xE000C00C			;
	MOV r1, #3					;
	STRB r1, [r0]				;
	LDMFD sp!, {r0, r1, lr}
	BX lr

;=====================================
;INTERRUPT Initialization
;=====================================
interrupt_init
		STMFD SP!, {r0-r3, lr}   ; Save registers

		; Push button setup
		LDR r0, =0xE002C000
		LDR r1, [r0]
		ORR r1, r1, #0x20000000
		BIC r1, r1, #0x10000000
		STR r1, [r0]  ; PINSEL0 bits 29:28 = 10
	
		;sets the match register
		LDR r0, =0xE000401C 
		LDR r1, =0x00465000				; .25 seconds	
		STR r1, [r0]

		; Classify sources as IRQ or FIQ
		LDR r0, =0xFFFFF000
		LDR r1, [r0, #0xC]
		ORR r1, r1, #0x8000 ; External Interrupt 1
		ORR r1, r1, #0x0040 ;UART INTERUPT
		ORR r1, r1, #0x0010 ;Timer
		STR r1, [r0, #0xC]

		; Enable Interrupts
		LDR r0, =0xFFFFF000
		LDR r1, [r0, #0x10]
		ORR r1, r1, #0x8000 ; External Interrupt 1
		ORR r1, r1, #0x0040 ;UART INTERUPT
		ORR r1, r1, #0x0010 ;Timer
		STR r1, [r0, #0x10]

		; External Interrupt 1 setup for edge sensitive
		LDR r0, =0xE01FC148
		LDR r1, [r0]
		ORR r1, r1, #2  ; EINT1 = Edge Sensitive
		STR r1, [r0]
		
		; UART Interrupt  setup
		LDR r0, =0xE000C004
		LDR r1, [r0]
		ORR r1, r1, #1  ;bit 0 = RDA =1
		STR r1, [r0]

		;Enable the settings for the timer to interrupt
		LDR r0, =0xE0004014
		LDR r1, [r0]
		ORR r1, r1, #24  ;Enable the timer
		STR r1, [r0]

		;Set the timer
		LDR r0, =0xE0004004
		LDR r1, [r0]
		ORR r1, r1, #1  
		STR r1, [r0]

		; Enable FIQ's, Disable IRQ's
		MRS r0, CPSR
		BIC r0, r0, #0x40
		ORR r0, r0, #0x80
		MSR CPSR_c, r0

		LDR r2, =timeCount
		MOV r3, #0
		STRB r3, [r2]

		LDMFD SP!, {r0-r3, lr} ; Restore registers
		BX lr             	   ; Return

;===============================================================
;	game setup
;===============================================================
game_setup
	STMFD sp!, {r0-r3, lr}

	LDR r1, =lives	; setting up lives
	MOV r2, #4
	STRB r2, [r1]

	LDR r1, =enemycount	; there are 3 enemies
	MOV r2, #3
	STRB r2, [r1]

	LDR r1, =level
	MOV r2, #1
	STRB r2, [r1]

	LDR r1, =bombTimer
	MOV r2, #0
	STRB r2, [r1]

	LDR r4, =brickcount
	MOV r2, #0
	STRB r2, [r4] 

	LDR r4, =scoreCount
	MOV r5, #0
	STRB r5, [r4]

	LDMFD sp!, {r0-r3, lr}
	BX lr

	
;===============================================================
;	create board
;===============================================================
create_board
		STMFD sp!, {r0-r11, lr}
			LDR r10, =position ;Load memory address for position
			MOV r11, #53 ; set inital value for the display
			STR r11, [r10]
			LDR r0, =board
			MOV r3, #'B'
			STRB r3, [r0, r11]

			LDR r10, =positionEnemy1 ;Load memory address for position
			MOV r11, #95 ; set inital value for the display
			STR r11, [r10]
			LDR r0, =board
			MOV r3, #'x'
			STRB r3, [r0, r11]

			LDR r10, =positionEnemy2 ;Load memory address for position
			LDR r11, =788 ; set inital value for the display
			STR r11, [r10]
			LDR r0, =board
			MOV r3, #'x'
			STRB r3, [r0, r11]

			LDR r10, =positionBoss ;Load memory address for position
			LDR r11, =830; set inital value for the display
			STR r11, [r10]
			LDR r0, =board
			MOV r3, #'X'
			STRB r3, [r0, r11]

			LDR r10, =enemy1Dir
			MOV r11, #1 ; set inital value for the display
			STRB r11, [r10]

			LDR r10, =enemy2Dir
			MOV r11, #2 ; set inital value for the display
			STRB r11, [r10]



			LDR r10, =save;					ends the line	 +
			BL output_string

			LDR r10, =intro1;					ends the line	 +
			BL output_string

			LDR r10, =intro2;					ends the line	 +
			BL output_string

			LDR r10, =board
			BL output_string

		LDMFD sp!, {r0-r11, lr}
		BX lr

;===============================================================
;	render and draw board
;===============================================================
render_and_draw
		STMFD sp!, {r0-r12, lr}

			LDR r1, = positionEnemy1 ;Load the memory address the position of Bobmberman
			LDR r2, [r1]	;load the value from the register
			LSR r2, r2, #16

			LDR r4, =board
			MOV r1, #'x'
			STRB r1, [r4,r2]
			
			LDR r1, = positionEnemy2 ;Load the memory address the position of Bobmberman
			LDR r2, [r1]	;load the value from the register
			
			LDR r4, =board
			MOV r1, #'x'
			STRB r1, [r4,r2]

			LDR r1, = positionBoss ;Load the memory address the position of Bobmberman
			LDR r2, [r1]	;load the value from the register
			LSR r2, r2, #16

			LDR r4, =board
			MOV r1, #'X'
			STRB r1, [r4,r2]

			

		 	LDR r1, = position ;Load the memory address the position of Bobmberman
			LDR r2, [r1]	;load the value from the register
			;LSR r2, r2, #24

			LDR r4, =board
			LDRB r1, [r4,r2]
			CMP r1, #'B'
			BNE check
			B good
check
			CMP r1, #'x'  ;check for normal enemy
			BEQ loselife
			CMP r1, #'X'
			BEQ loselife
			CMP r1, #' '
			BNE Exit
good
			MOV r3, #'B'
			STRB r3, [r4, r2]		

			; do score AFTER bomb
			
			; make sure to go to Exit

			LDR r10, =intro1;					ends the line	 +
			BL output_string

			LDR r10, =intro2;					ends the line	 +
			BL output_string

			LDR r10, =StatusUP
			BL output_string

			LDR r10, =board
			BL output_string

			B Exit
loselife
	BL lose_life

Exit
		LDMFD sp!, {r0-r12, lr}
		BX lr



;===============================================================
;	paused screen
;===============================================================
pausedScreen
	STMFD sp!, {r0-r12, lr}
	LDR r10, =pausedNOTE
	BL output_string
		
	LDR r10, =introP;					ends the line	 +
	BL output_string

	LDR r10, =intro2;					ends the line	 +
	BL output_string

	LDR r10, =StatusUP
	BL output_string

	LDR r10, =board
	BL output_string
	LDMFD sp!, {r0-r12, lr}
	BX lr

;===============================================================
;	bombcheck
;===============================================================
bombCheck
	STMFD sp!, {r0-r2, lr}
	LDR r1, =bombflag
	LDRB r2, [r1]
	CMP r2, #1
	BEQ placebomb

return_bombplant

	LDMFD sp!, {r0-r2, lr}
	BX lr

placebomb
	LDR r1, =position
	LDR r2, [r1]		 ; this is the issue

	LDR r3, =board
	MOV r5, #'o'
	STRB r5, [r3, r2]

	LDR r1, bombTimer
	LDRB r2, [r1]
	CMP r2, #5
	BEQ explosion
	ADD r2, r2, #1
	STRB r2, [r1]
	B return_bombplant

explosion
	; enter the directional checks here

	;end the directional checks here
	LDR r1, =bombflag
	MOV r2, #0
	STRB r2, [r1]
	B return_bombplant
	
;===============================================================
;	timer
;===============================================================
timerCheck
	STMFD sp!, {r1-r7, lr}
	LDR r4, =timeCount
	LDRB r5, [r4]
	CMP r5, #4 
	BEQ timer_dec

	LDR r4, =timeCount
	LDRB r5, [r4]
	ADD r5, r5, #1
	STRB r5, [r4]
	B loadback

timer_dec
	LDR r1, =StatusUP	
	LDRB r5, [r1, #10]		; loads the singles digit
	CMP r5, #0x30
	BEQ downTen

	MOV r6, #1
	SUB r5, r5, r6
	STRB r5, [r1, #10]
	MOV r5, #0
	LDR r6, =timeCount
	STRB r5, [r6] 
	B loadback

downTen	
	MOV r6, #0x39
	STRB r6, [r1, #10]	; stores a 9 in the singles digit of timer
	
	LDRB r5, [r1, #9]		; loads the ten's digit
	CMP r5, #0x30
	BEQ downHund

	MOV r6, #1
	SUB r5, r5, r6
	STRB r5, [r1, #9]
	MOV r5, #0
	LDR r6, =timeCount
	STRB r5, [r6] 
	B loadback

downHund
	MOV r6, #0x39
	STRB r6, [r1, #9]	; stores a 9 in the ten's digit
	
	LDRB r5, [r1, #8]  ; loads the hundreds digit
	CMP r5, #0x30
	BEQ gameoverTransition

	MOV r6, #1
	SUB r5, r5, r6
	STRB r5, [r1, #8]
	MOV r5, #0
	LDR r6, =timeCount
	STRB r5, [r6] 
	B loadback

loadback
	LDMFD sp!, {r1-r7, lr}
	BX lr

gameoverTransition
	LDMFD sp!, {r1-r7, lr}
	B gameoverTime



;===============================================================
;	random_init
;===============================================================
random_init
	STMFD sp!, {r3-r5, lr}		
	LDR r5, =rand ;Load memory address
	MOV r3, #1 ; set inital value for the display
	STRB r3, [r5]
	LDR r5, =rand2 ;Load memory address
	MOV r3, #1 ; set inital value for the display
	STRB r3, [r5]
   LDMFD sp!, {r3-r5, lr}
   BX lr



;===============================================================
;	random
;===============================================================
random
	STMFD sp!, {r3-r5, lr}
	LDR r5, =rand ;Load memory address
	LDRB r3, [r5]

	LDR r4, =rand2 ;Load memory address
	LDRB r6, [r4]
	CMP r3, #7
	BGE nvm1
	ADD r3, r6
nvm1

	ADD r3, r3, #1 ; increment counter
	CMP r3, #15
	BEQ limit
	STRB r3, [r5]
	B end_rand
limit
	MOV r3, #0
	STRB r3, [r5]
end_rand
	LDMFD sp!, {r3-r5, lr}
	BX lr

;===============================================================
;	random2
;===============================================================
random2
	STMFD sp!, {r3-r5, lr}
	LDR r5, =rand2 ;Load memory address
	LDRB r3, [r5]

	LDR r4, =rand ;Load memory address
	LDRB r6, [r4]
	CMP r6, #3
	BGE nvm
	ADD r3, r6
nvm
	ADD r3, r3, #1 ; increment counter
	CMP r3, #7
	BEQ limit2
	STRB r3, [r5]
	B end_rand2
limit2
	MOV r3, #0
	STRB r3, [r5]
end_rand2
	LDMFD sp!, {r3-r5, lr}
	BX lr
;===============================================================
;	move bomberman
;===============================================================
move_bomberman
		STMFD sp!, {r2-r12, lr}
		CMP r1, #'w'
		BEQ	 UP

		CMP r1, #'a'
		BEQ	 LEFT

		CMP r1, #'s'
		BEQ	 DOWN

		CMP r1, #'d'
		BEQ	 RIGHT

		CMP r1, #'b'
		BEQ BOMBS

		B end_move

UP
		LDR r10, = position ;Load the memory address the count is stored in
		LDRH r2, [r10]	;load the value from the register
		LDR r0, =board

		SUB r3, r2, #49
		LDRB r1, [r0,r3]
		CMP r1, #' '
		BNE end_move

		MOV r3, #' '
		STRB r3, [r0, r2]
		SUB r3, r2, #49
			
		STR r3, [r10]
		B end_move
DOWN
   		LDR r10, = position ;Load the memory address the count is stored in
		LDRH r2, [r10]	;load the value from the register
		LDR r0, =board

		ADD r3, r2, #49
		LDRB r1, [r0,r3]
		CMP r1, #' '
		BNE end_move

		MOV r3, #' '
		STRB r3, [r0, r2]
		ADD r3, r2, #49
		STR r3, [r10]
		B end_move
LEFT
		LDR r10, = position ;Load the memory address the count is stored in
		LDRH r2, [r10]	;load the value from the register
		LDR r0, =board

		SUB r3, r2, #2
		LDRB r1, [r0,r3]
		CMP r1, #' '
		BNE end_move

		MOV r3, #' '
		STRB r3, [r0, r2]
		SUB r3, r2, #3
		STR r3, [r10]
		B end_move
RIGHT
		LDR r10, = position ;Load the memory address the count is stored in
		LDRH r2, [r10]	;load the value from the register
		LDR r0, =board

		ADD r3, r2, #2
		LDRB r1, [r0,r3]
		CMP r1, #' '
		BNE end_move

		MOV r3, #' '
		STRB r3, [r0, r2]
		ADD r3, r2, #3
		STR r3, [r10]
		B end_move

BOMBS
		LDR r10, =bombflag
		MOV r1, #1 ; if a 1 has been pressed, then the flag is set
		STRB r1, [r10]
		B end_move

end_move
		LDMFD sp!, {r2-r12, lr}
		BX lr

;===============================================================
;	move enemy
;===============================================================
move_enemy
		STMFD sp!, {r0-r12, lr}
		LDR r10, =rand
		LDRB r4, [r10]
		CMP r4, #5
		BLT	start_me
		LDR r10, =enemy1Dir
		LDRB r4, [r10]
start_me
		LDR r10, = positionEnemy1 ;Load the memory address for enemy 1
		LDR r2, [r10]	;load the value from the register
		LSR r2, #16
		LDR r0, =board

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]

		CMP r4, #1
		BEQ enemy_left
		CMP r4, #2
		BEQ enemy_right
		CMP r4, #3
		BEQ enemy_down
		CMP r4, #4
		BEQ enemy_up
enemy_left
		;check left
		SUB r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose
		CMP r3, #' '
		BEQ advance_1
		CMP r3, #'x'
		BEQ end_move_enemy
		LDR r5, =enemy1Dir
		MOV r11, #2 ; set inital value for the display
		STRB r11, [r5]
		B enemy_right

enemy_right
		;check right
		ADD r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose
		CMP r3, #' '
		BEQ advance_1
		CMP r3, #'x'
		BEQ end_move_enemy
		LDR r5, =enemy1Dir
		MOV r11, #3 ; set inital value for the display
		STRB r11, [r5]
		B enemy_down

enemy_down
		;check down
		ADD r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose
		CMP r3, #' '
		BEQ advance_1
		CMP r3, #'x'
		BEQ end_move_enemy
		LDR r5, =enemy1Dir
		MOV r11, #4 ; set inital value for the display
		STRB r11, [r5]
		B enemy_up

enemy_up
		;check up
		SUB r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose
		CMP r3, #' '
		BEQ advance_1
		CMP r3, #'x'
		BEQ end_move_enemy
		LDR r5, =enemy1Dir
		MOV r11, #1 ; set inital value for the display
		STRB r11, [r5]
		B enemy_left
lose
		BL lose_life		
advance_1
		STR r4, [r10]

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]



end_move_enemy
		LDMFD sp!, {r0-r12, lr}
		BX lr


;===============================================================
;	move enemy2
;===============================================================
move_enemy2
		STMFD sp!, {r0-r12, lr}
		LDR r10, =enemy2Dir
		LDRB r4, [r10]

	;	LDR r10, =rand ;Load memory address
		;LDR r4, [r10]
	;	LSR r4, r4, #24

		LDR r10, = positionEnemy2 ;Load the memory address for enemy 1
		LDR r2, [r10]	;load the value from the register
		LDR r0, =board

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]

		CMP r4, #1
		BEQ enemy2_left
		CMP r4, #2
		BEQ enemy2_right
		CMP r4, #3
		BEQ enemy2_down
		CMP r4, #4
		BEQ enemy2_up
enemy2_left
		;check left
		SUB r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose2
		CMP r3, #' '
		BEQ advance_2
		LDR r5, =enemy2Dir
		MOV r11, #2 ; set inital value for the display
		STRB r11, [r5]
		B enemy2_right

enemy2_right
		;check right
		ADD r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose2
		CMP r3, #' '
		BEQ advance_2
		LDR r5, =enemy2Dir
		MOV r11, #3 ; set inital value for the display
		STRB r11, [r5]
		B enemy2_down

enemy2_down
		;check down
		ADD r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose2
		CMP r3, #' '
		BEQ advance_2
		LDR r5, =enemy2Dir
		MOV r11, #4 ; set inital value for the display
		STRB r11, [r5]
		B enemy2_up

enemy2_up
		;check up
		SUB r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose2
		CMP r3, #' '
		BEQ advance_2
		LDR r5, =enemy2Dir
		MOV r11, #1 ; set inital value for the display
		STRB r11, [r5]
		B enemy2_left
lose2
		BL lose_life		
advance_2
		STR r4, [r10]

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]



end_move_enemy2
		LDMFD sp!, {r0-r12, lr}
		BX lr
		
;===============================================================
;	move boss
;===============================================================
move_boss
		STMFD sp!, {r0-r12, lr}
		LDR r10, =bossDir
		LDRB r4, [r10]

		LDR r10, = positionBoss ;Load the memory address for enemy 1
		LDR r2, [r10]	;load the value from the register
		LSR r2, r2, #16

		LDR r0, =board

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]

		CMP r4, #1
		BEQ boss_left
		CMP r4, #2
		BEQ boss_right
		CMP r4, #3
		BEQ boss_down
		CMP r4, #4
		BEQ boss_up
boss_left
		;check left
		SUB r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose3
		CMP r3, #' '
		BEQ advance_3
		LDR r5, =bossDir
		MOV r11, #2 ; set inital value for the display
		STRB r11, [r5]
		B boss_right

boss_right
		;check right
		ADD r4, r2, #3
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose3
		CMP r3, #' '
		BEQ advance_3
		LDR r5, =bossDir
		MOV r11, #3 ; set inital value for the display
		STRB r11, [r5]
		B boss_down

boss_down
		;check down
		ADD r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose3
		CMP r3, #' '
		BEQ advance_3
		LDR r5, =bossDir
		MOV r11, #4 ; set inital value for the display
		STRB r11, [r5]
		B boss_up

boss_up
		;check up
		SUB r4, r2, #49
		LDRB r3, [r0, r4]
		CMP r3, #'B'
		BEQ lose3
		CMP r3, #' '
		BEQ advance_3
		LDR r5, =bossDir
		MOV r11, #1 ; set inital value for the display
		STRB r11, [r5]
		B boss_left
lose3
		BL lose_life		
advance_3
		STR r4, [r10]

		;clear old position
		MOV r3, #' '
		STRB r3, [r0, r2]



end_move_enemy3
		LDMFD sp!, {r0-r12, lr}
		BX lr
				
;===============================================================
;	Lose Life
;===============================================================
lose_life
		STMFD sp!, {r0-r12, lr}

		LDR r10, =lolife
		BL output_string

		LDR r10, =lives
		LDRB r1, [r10]
		MOV r2, #1
		SUB r1, r1, r2
		STRB r1, [r10]

		BL led

		LDR r10, =position ;Load memory address for position
		MOV r11, #53 ; set inital value for the display
		STR r11, [r10]
		LDR r0, =board
		MOV r3, #'B'
		STRB r3, [r0, r11]

		LDMFD sp!, {r0-r12, lr}
		BX lr

;===============================================================
;	gameover
;===============================================================
gameover
	STMFD sp!, {r0-r12, lr}
	LDR r0, =0xE0028018
	LDR r1, [r0]
	AND r1, r1, #0
	STR r1, [r0] ; PINSEL0 bits 29:28 = 10
gameoverTime
	;enabling purple rgb - game ended
	LDR r0, =0xE0028008	 ;rgb register
	LDR r1, [r0]
	MOV r1, #0x00060000  ;purple
	STR r1, [r0] ;

	LDR r10, =gameover1
	BL output_string
	LDMFD sp!, {r0-r12, lr}
	B endGame


;===============================================================
;	score
;===============================================================
score_up
	STMFD sp!, {r0-r12, lr}
	LDR r4, =scoreCount
	LDRB r5, [r4]
	ADD r5, r5, #1
	STRB r5, [r4]
	;B load

score_dec
	LDR r1, =StatusUP	
	LDRB r5, [r1, #47]		; loads the singles digit
	CMP r5, #0x39
	BEQ upTen

	MOV r6, #1
	ADD r5, r5, r6
	STRB r5, [r1, #47]
	MOV r5, #0
	LDR r6, =scoreCount
	STRB r5, [r6] 
	B load						  

upTen	
	MOV r6, #0x30
	STRB r6, [r1, #47]	; stores a 9 in the singles digit of timer
	
	LDRB r5, [r1, #46]		; loads the ten's digit
	CMP r5, #0x39
	BEQ downHun

	MOV r6, #1
	ADD r5, r5, r6
	STRB r5, [r1, #46]
	MOV r5, #0
	LDR r6, =scoreCount
	STRB r5, [r6] 
	B load

downHun
	MOV r6, #0x39
	STRB r6, [r1, #46]	; stores a 9 in the ten's digit
	
	LDRB r5, [r1, #45]  ; loads the hundreds digit


	MOV r6, #1
	ADD r5, r5, r6
	STRB r5, [r1, #45]
	MOV r5, #0
	LDR r6, =scoreCount
	STRB r5, [r6] 
	B load

load
	LDMFD sp!, {r0-r12, lr}
	BX lr

;===============================================================
;	level up
;===============================================================
level_up
	STMFD sp!, {r0-r12, lr}

	LDR r4, =level
	LDRB r5, [r4]
	ADD r5, r5, #1
	STRB r5, [r4]

	LDR r1, =0xE002800C	; clear address
	LDR r2, =0x00003F80	; pins to turn on
	STR r2, [r1] ;set pins
	LDR r1, =0xE0028008	; direction address
	LDR r2, =0x00003F80	; pins to turn on
	STR r2, [r1] ;set pins
	LDR r1, =0xE0028000 ; Base address
	LDR r3, =digits_SET
	MOV r0,	r5
	MOV r0, r0, LSL #2 ; Each stored value is 32 bits
	LDR r2, [r3, r0] ; Load IOSET pattern for digit in r0
	STR r2, [r1, #4] ; Display (0x4 = offset to IOSET)

	LDR r4, = position ;Load the memory address the count is stored in
	LDR r2, [r4]	;load the value from the register
	LDR r5, =board
	MOV r1, #' '
	STRB r1, [r5,r2]

	LDR r4, = positionEnemy1 ;Load the memory address the count is stored in
	LDR r2, [r4]	;load the value from the register
	LSR r2, r2, #16
	LDR r5, =board
	MOV r1, #' '
	STRB r1, [r5,r2]

	LDR r4, = positionEnemy2 ;Load the memory address the count is stored in
	LDR r2, [r4]	;load the value from the register
	LDR r5, =board
	MOV r1, #' '
	STRB r1, [r5,r2]

	LDR r4, = positionBoss ;Load the memory address the count is stored in
	LDR r2, [r4]	;load the value from the register
	LSR r2, r2, #16
	LDR r5, =board
	MOV r1, #' '
	STRB r1, [r5,r2]

	LDR r4, =level
	LDRB r5, [r4]
	MOV r2, #10
	MUL r6, r5, r2
brick_loop
	CMP r6 , #0
	BEQ done_brick_loop
	SUB r6, r6, #1
	BL random2
	BL random2
	BL random
	BL generate_brick
	B brick_loop
done_brick_loop
	BL create_board

	;sets the match register
	;LDR r0, =0xE000401C 
	;LDR r1, =0x00465000				; .25 seconds
	;MOV r2, #888
	;MUL r6, r5, r2
	;SUB r1, r1, r6	
	;STR r1, [r0]

	;sets the timer
	;LDR r0, =0xE0004004
	;LDR r1, [r0]
	;ORR r1, r1, #1
	;STR r1, [r0]

	LDMFD sp!, {r0-r12, lr}
	BX lr

;===============================================================
;	Generte Brick
;===============================================================
generate_brick
    STMFD sp!, {r2-r7, lr}
    ;rowNum = rand[0,15]
    LDR r5, =rand
    LDRB r2, [r5]

    ;horizontal_offset=6
    MOV r3, #6
    ;if rowNum=0 or rowNum%3=0
    CMP r2, #0
    BEQ inside

	MOV r0, r2
	MOV r1, #3 
    BL division
    CMP r1, #0
    BNE doneBCheck
inside
    ;if rowNum >= 7 rowNum-8
    LDR r5, =rand2
    LDRB r6, [r5]
    CMP r6, #7
    BLE generate_brick_position
    SUB r2, r2, #8
    B generate_brick_position

doneBCheck    ;else col offset = 3
    MOV r3, #3

generate_brick_position
    ;Block Position = rowNum * 49(vertical offset) + horizontal_offset * rand2 + 52(first space)

    MUL r3, r6, r3
	MOV r7, #49
    MUL r4, r2, r7
    ;add to the first Space
    ADD r2, r3, r4
	ADD r2, r2, #53

    ;load brick onto board with the block Position as offset
   	;LDR r3, =wall
	;LDR r3, [r3]
	MOV r3, #0x23
    LDR r5, =board
	LDRB r4, [r5,r2]
	CMP r4, #' '
	BNE end_gen
    STRB r3, [r5,r2]
	;increment the brick counter
	LDR r4, =brickcount
	LDRB r5, [r4]
	ADD r5, r5, #1
	STRB r5, [r4] 
end_gen
    LDMFD sp!, {r2-r7, lr}
    BX lr
;==========
;===============================================================
;	READ STRING
;===============================================================
read_string
		STMFD sp!, {r0-r3, lr}
		MOV r0, #0					;initialize Counter
read_loop
		b read_character
return_rc
		b output_character
return_oc
		MOV r4, #0x0D 				;Move return into r4
		CMP r4, r5					;Compare to r5
		BEQ done_read				;If enter was pressed stop
		STRB r5, [r10, r11]			;r5 is the value returned from the recive register
		ADD r11, r11, #1			;increment the counter
		B read_loop					;
done_read
		MOV r9, #0x00
		STRB r9, [r10, r11]
		MOV r5 , #0x0A				;Move new line  into r5
		LDR r4, =0xE000C000			;Load recive register
		STRB r5, [r4]			   	;Store in transmit register

		LDMFD sp!, {r0-r3, lr}
		BX lr
;===============================================================

;===============================================================
;	RECEIVE
;===============================================================
read_character
		LDR r0, =0xE000C014			;Load U0LSR in r0
		LDRB r1, [r0]
		MOV r2, #1
		AND r3, r1, r2
		 BL random
		 BL random2
		 BL generate_brick
		CMP r3,#0					;Check if RDR is 1
		BEQ read_character			;Branch if RDR=1 ;Else Loop
		LDR r4, =0xE000C000			;Load recive register
		LDRB r5, [r4]
		B return_rc
;===============================================================

;===============================================================
;	OUTPUT CHARACTER
;===============================================================

output_character
		LDR r6, =0xE000C014
		LDRB r7, [r6]
		MOV r8, #32					; creates new register with only a 1 in THRE
		AND r9, r7, r8 				; ands the register together
		CMP r9, #32
		BEQ SHOP
		B output_character
SHOP
		LDR r4, =0xE000C000				;Load recive register
		STRB r5, [r4]
		B return_oc
;===============================================================

;===============================================================
;	OUTPUT STRING
;===============================================================
output_string
		STMFD sp!, {r0-r9, lr}

		LDR r1, =0xE000C000			;Load recive register
		MOV r0, #0
out_str_loop
output_c
		LDR r6, =0xE000C014
		LDRB r7, [r6]
		MOV r8, #32					; creates new register with only a 1 in THRE
		AND r9, r7, r8 				; ands the register together
		CMP r9, #32
		BEQ SHP
		B output_c
SHP
		LDRB r2, [r10, r0]  		;Load String offset by count
		STRB r2, [r1]			   	;Store in transmit register
		ADD r0, r0, #1				;Increment the counter
		CMP r2, #0				  	;check for null
		BNE out_str_loop


		LDMFD sp!, {r0-r9, lr}
		BX lr

;===========================================================================================
; PARSE INT
;===========================================================================================
singleDigit
	MOV r5, r4		; copies dividend into r5
	MOV r11, r5
	B returnSD
skipping
	add r0, r0, #1
	B int_loop

parse_int
	STMFD sp!, {r0-r7, lr}
	LDR r0, =string
	STR r10, [r0]

int_loop
	LDRB r1,[r0]
	MOV r11, #0x00	; this is ASCII for NULL
	CMP r1, #0x20
	BEQ skipping

	SUB r4, r1, #0x30			; gets the decimal number
	ADD r0, r0, #1
iter_loop
	LDRB r1, [r0]
	CMP r1, r11				; is the next value a NULL?
	BEQ singleDigit

	MOV r8, #10
	MUL r5, r4, r8			; multiplies the number by 10

	SUB r4, r1, #0x30			; gets the decimal number
	ADD r5, r5, r4
	MOV r4, r5

	ADD r0, r0, #1
	LDRB r1, [r0]
	CMP r1, r11				; is the next value a NULL?
	BNE iter_loop

	MOV r11, r5				; copies the numerical value into r2
returnSD
	LDMFD sp!, {r0-r7, lr}
	BX lr

;===============================================================
;	HEX - SEVEN SEGMENT DISPLAY
;===============================================================
hex
	STMFD sp!, {r0-r7, lr}
	;pass digits_SET into r3
	;pass user input into r11
	;pins 7-13
	LDR r1, =0xE002800C	; clear address
	LDR r2, =0x00003F80	; pins to turn on
	STR r2, [r1] ;set pins
	LDR r1, =0xE0028008	; direction address
	LDR r2, =0x00003F80	; pins to turn on
	STR r2, [r1] ;set pins
	LDR r1, =0xE0028000 ; Base address
	LDR r3, =digits_SET
	LDR r5, =level
	LDRB r0, [r5]
	MOV r0, r0, LSL #2 ; Each stored value is 32 bits
	LDR r2, [r3, r0] ; Load IOSET pattern for digit in r0
	STR r2, [r1, #4] ; Display (0x4 = offset to IOSET)

	LDMFD sp!, {r0-r7, lr}
	BX lr


;===============================================================
;	LED - LIGHT EMITTING DIODE
;===============================================================
led
	STMFD sp!, {r4-r12, lr}
	LDR r1, =lives
	LDRB r2, [r1]

	CMP r2, #0
	BEQ gameover	

	CMP r2, #1
	BEQ one_life

	CMP r2, #2
	BEQ two_life

	CMP r2, #3
	BEQ three_life

	CMP r2, #4
	BEQ four_life

	B end_led

one_life
	MOV r11, #8
	B end_led	

two_life   	
	MOV r11, #12
	B end_led

three_life  	
	MOV r11, #14
	B end_led

four_life
	MOV r11, #15

end_led   	  	
	LSL r11, #16
	
		LDR r0, =0xE0028018
		LDR r1, [r0]
		AND r1, r1, #0
		STR r1, [r0] ; PINSEL0 bits 29:28 = 10
		;connect to port 1 SET Address: 0xE0028014, DIR Address: 0xE0028018  , CLEAR Address:0xE002801C  , VALUE Address:0xE0028010 
		;pin 16 is the MSB while the
		;pin 19 is the LSB. 
		;IO0DIR = 0x0007 8000 ;pin P1.16-19 configured as output
		LDR r0, =0xE0028018
		LDR r1, [r0]
		ORR r1, r1, r11
		STR r1, [r0] ; PINSEL0 bits 29:28 = 10 

		;0000 0000 0000 0111 1000 0000 0000 0000 	 light all leds
		;0x00078000
		;pin 16 is the MSB while the
		;pin 19 is the LSB. 
	LDMFD sp!, {r4-r12, lr}
	BX lr

;===============================================================
;	CLEAR DISPLAY
;===============================================================
clearDisp
	STMFD sp!, {r0-r7, lr}
	LDR r10, = clear1
	BL output_string
	BL hex
	LDMFD sp!, {r0-r7, lr}
	BX lr
;===============================================================




;===============================================================
;	CLEAR DISPLAY
;===============================================================
turnOFF
	STMFD sp!, {r0-r7, lr}
	LDR r10, =turnOFFprompt
	BL output_string
	LDR r10, =turnOFFprompt2
	BL output_string
	LDMFD sp!, {r0-r7, lr}
	BX lr

;===============================================================

;===============================================================
;	CLEAR DISPLAY
;===============================================================
turnON
	STMFD sp!, {r0-r7, lr}
	LDR r10, =turnONprompt
	BL output_string
	LDMFD sp!, {r0-r7, lr}
	BX lr
;===============================================================

;===============================================================
;	CLEAR DISPLAY
;===============================================================
randomNum
	STMFD sp!, {r0-r7, lr}
	LDR r10, =random1
	BL output_string
	LDMFD sp!, {r0-r7, lr}
	BX lr
;===============================================================
division
	STMFD sp!, {r2-r9, lr}			
;The dividend is passed in r0 and the divisor in r1.
			MOV r6, #0						;Initialize Neg dividend check
			MOV r9, #0						;Initialize Neg divisor check
			MOV r1, #2
;Negative Divisor Check
					CMP r1,#0						;If r8 is negative
					BMI NDIVISOR					;Branch 
					B CONT							;Else Continue to Dividend check
NDIVISOR		MOV r9, #1					;Set divisor Negative Flag
					MVN r1,r1							;Take the one's	Complent of divisor
					ADD r1, r1, #1					;Add one to get the answer


;Negative Dividend Check
CONT			CMP r0,#0						;If sum is negative
					BMI NDIVIDEND				;Branch
					B START							;Else Continue to Start
NDIVIDEND	MOV r6,#1				;Set Negative Flag	
					MVN r0,r0							;Take the one's	Complent of dividend
					ADD r0, r0, #1					;Add one to get the answer


;Divison routine for unsigned Integers
START			MOV r2,#15					;Initialize Counter
					MOV r5, #0						;Initailize Quotient
					MOV r3, r1, LSL #15 			;Divisor
					MOV r4, r0						;Initialize remainder to dividend
LOOP
					SUB r4,r4, r3						;Remainder := Remainder - Divisor
					CMP r4, #0						;Is Remainder = 0?
					BLT IF								;If(Remainder < 0)
					MOV r5, r5 , LSL #1			;Left Shift Quotient
					ADD r5, r5, #1					;LSB=1
					B ELSE
IF		
					ADD r4, r4	, r3					;Remainder :=Remainder + Divisor
					LSL r5, #1							;Left Shift Quotient LSB=0

ELSE	
				LSR r3, #1						;Right Shift Diviisor MSB=0
				CMP r2, #0						;Is Counter = 0
				BLE BREAK						;If(Counter > 0)
				SUB r2, r2, #1					;Decrement Counter
				B LOOP

BREAK		CMP r6,r9						;Compare Negative Flags
				BNE NEG							;If not equal branch and Negate the Answer
				MOV r0,r5						;Move quotient into r0
				B FINISH				
NEG			MVN r0,r5						;Take the one's	Complent of quotient
			ADD r0, r0, #1					;Add one to get the answer

FINISH		MOV r1,r4						;Copy remainder into r1
	LDMFD sp!, {r2-r9, lr}
	BX lr
;The quotient is returned in r0 and the remainder in r1.
	END