## Setting up git##
```
#!shell

cd ~/Windows/[CSE 380 Folder]/[Lab Folder]
or
mkdir  ~/Windows/CSE380/[Lab#]
cd  ~/Windows/CSE380/[Lab#]
git init
git remote add origin https://[username]@bitbucket.org/cse380/lab7.git
```
## Creating a Branch ##
```
#!shell

git pull origin master
git checkout -b [name_of_your_new_branch]
git push origin [name_of_your_new_branch]
```
## Commiting Changes ##
```
#!shell

git pull origin master
git add [List of files changed]
git commit -m 'Commit message of what was done'
git push origin master
if it blows up
git push --set-upstream origin master
```