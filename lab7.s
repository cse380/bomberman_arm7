	AREA	GPIO, CODE, READWRITE	
	EXPORT lab7
	EXPORT FIQ_Handler
	IMPORT output_string
	IMPORT pin_connect_block_setup_for_uart0
	IMPORT uart_init
	IMPORT read_string
	IMPORT interrupt_init
	IMPORT create_board
	IMPORT render_and_draw
	IMPORT move_bomberman
	IMPORT lose_life
	IMPORT move_enemy
	IMPORT move_enemy2
	IMPORT move_boss
	IMPORT random_init
	IMPORT random
	IMPORT random2
	IMPORT bombCheck
	IMPORT timerCheck
	IMPORT led
	IMPORT game_setup
	IMPORT hex
	IMPORT generate_brick
	IMPORT pausedScreen
	IMPORT level_up
	IMPORT score_up
	EXPORT endGame
	
;=====================================================================================================
;PROMPT
;=====================================================================================================

positionPlayer = " ", 0
speedcount1 = " "
positionBoss = " ", 0
movecount1 = " " , 0
gameover1 = 0xA, 0xD, "gameover!", 0xA, 0xD, 0
direction = " ", 0
time1 = 0xA, 0xD, "       Time:120", 0
score	= 0xA,0xD,"       SCORE:  0000  ",0
slowFlag = " " , 0
start = 0xA, 0xD,  "                       	     .-.                " 
	  = 0xA, 0xD,  "                             '-'                "
	  = 0xA, 0xD,  "                             //                 " 
      = 0xA, 0xD,  "                    _..---._/|                  " 
	  = 0xA, 0xD,  "                  .' .     '-.                  " 
	  = 0xA, 0xD,  "			              ////         \\\\              " 
	  = 0xA, 0xD,  " 			             ===_____     __|               "  
	  = 0xA, 0xD,  "			            //#  #   |   //()\\\\            " 
	  = 0xA, 0xD,  "			            ||#  #   |   \\\\__//            " 
	  = 0xA, 0xD,  "		            	\\\\#__#___|    //               " 
	  = 0xA, 0xD,  "			             \\\\___________|                " 
	  = 0xA, 0xD,  "       ()       ////\\||||))))                 " 
	  = 0xA, 0xD,  "      .(\\\\_     \\\\_\\\\////  |||||||                " 
	  = 0xA, 0xD,  "     ( )  |^|^|^|ooo//     ||    |             " 
	  = 0xA, 0xD,  "     (_)_.'v|v|v|     //   |||||||                  " 
	  = 0xA, 0xD,  "               //____//    ||____|omerMan              " 
      = 0xA, 0xD,  "               |_____//  _//  \\/               " 
      = 0xA, 0xD,  "			            //    \\\\__ |                 "	
      = 0xA, 0xD,  "               \\\\ -	//\\\\   \\\\          "
	  = 0xA, 0xD,  "             __// -_\\\\// -__\\\\             "
	  = 0xA, 0xD,  "            //-      //-     ||                "
	  = 0xA, 0xD,  "           ||||||||||||||||||||                "
      = 0xA, 0xD,  "           Press Enter To Play                 ", 0
pause = "      ", 0
	ALIGN
;=====================================================================================================	
; Start of main routine
;=====================================================================================================	

lab7
	STMFD SP!,{lr}	; Store register lr on stack
;================================================
; Initial set up
;================================================
	BL pin_connect_block_setup_for_uart0
	BL uart_init
	BL random_init

	LDR r10, =start;	
	BL output_string

   	BL read_string
	;LDR r1, =pause
	;MOV r0, #0
	;STRB r0, [r1]
	BL interrupt_init
	
	BL game_setup
	BL led
	BL hex

	MOV r5, #200
rand_loop 	
	BL random
	BL random2
	BL generate_brick
	BL random2
	SUB r5, r5, #1
	CMP r5, #0
	BEQ end_rand_loop

	B rand_loop
end_rand_loop
endGame	
	BL create_board	 ; create the board and set bomberman and initial positions

	

;no op eqiv
loop
	BL random
	BL random2
	
	ORR r0, r0, #0
	B loop
endGameblah
	LDMFD SP!, {lr}	; Restore register lr from stack	
	BX LR

;=====================================================================================================	
; Inturrupt	Handler
;=====================================================================================================	
FIQ_Handler
		STMFD SP!, {r0-r12, lr}   ; Save registers
;================================================
; Timer check
;================================================
TimeInit
		LDR r0, =0xE0004000
		LDR r1, [r0]
		CMP r1, #2
		BEQ Timer
;================================================
; Key press check
;================================================
INT1	; Check for EINT1 interrupt
		LDR r0, =0xE01FC140
		LDR r1, [r0]
		TST r1, #2
		BEQ INT
		
		STMFD SP!, {r0-r2, lr}   ; Save registers

		; Push button EINT1 Handling Code
;-----------------------------------------
; pause button
;-----------------------------------------
		LDR r1, =pause
		LDRB r2, [r1]
		CMP r2, #0
		BEQ pauseIT
		BNE unpauseIT

pauseIT
		LDR r0, =0xE0028008	 ;rgb register
		LDR r1, [r0]				;  wtf is this?
		ORR r1, #0x00040000  ; blue 
		STR r1, [r0] 

		LDR r1, =pause
		MOV r0, #1
		STRB r0, [r1]


		B continueP

unpauseIT
		LDR r1, =pause
		MOV r0, #0
		STRB r0, [r1]

		LDR r0, =0xE0028008	 ;rgb register
		LDR r1, [r0]				;  wtf is this?
		ORR r1, #0x00200000  ; green 
		STR r1, [r0] 
continueP
		LDMFD SP!, {r0-r2, lr}   ; Restore registers

		ORR r1, r1, #2		; Clear Interrupt
		STR r1, [r0]
		B FIQ_Exit
;================================================
; Key press handler
;================================================
INT
	   	LDR r0, =0xE000C008
		LDR r1, [r0]
		TST r1, #4
		BEQ FIQ_Exit
		
		LDR r0, =0xE000C000			;Load recive register  
		LDRB r1, [r0]	 		
		
		LDR r2, =pause
		LDRB r3, [r0]
		CMP r3, #1
		BEQ FIQ_Exit
		 
		BL move_bomberman
		CMP r1, #'q'
		BEQ	 asdf
		B contr
asdf
		BL score_up
contr
		BL bombCheck
		BL random
		BL random2
		B FIQ_Exit

;================================================
; Timer handler
;================================================
Timer
	STMFD SP!, {r0-r4, lr}

	;clear the interrupt
	LDR r0, =0xE0004000
	LDR r1, [r0]
	ORR r1, r1, #2
	STR r1, [r0]

	;sets the timer
	LDR r0, =0xE0004004
	LDR r1, [r0]
	ORR r1, r1, #1
	STR r1, [r0]

	LDR r0, =pause
	LDRB r1, [r0]
	CMP r1, #1
	BEQ END_TIME 

	LDR r1, =time1
	BL timerCheck
	BL random
	BL random2
	BL render_and_draw

	;move small enemies half the speed as boss
	LDR r4, =slowFlag
	LDRB r3, [r4]
	CMP r3, #1
	BEQ dont_move

	BL move_enemy
	BL move_enemy2

	MOV r3, #1
	STRB r3, [r4]
	B boss
dont_move
	MOV r3, #0
	STRB r3, [r4]
boss	
	BL move_boss
END_TIME
	LDMFD SP!, {r0-r4, lr}				 ;ends timer
	B FIQ_Exit	
		

			

FIQ_Exit
		LDMFD SP!, {r0-r12, lr}
		SUBS pc, lr, #4				   ;issue?
	END